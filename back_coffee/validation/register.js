const Validator = require('validator');
const isEmpty = require('./isEmpty');


module.exports = function validateRegisterInput(data) {
    let errors = {};

    data.login = !isEmpty(data.login) ? data.login : '';
    data.email = !isEmpty(data.email) ? data.email : '';
    data.password = !isEmpty(data.password) ? data.password : '';



    if (!Validator.isLength(data.login, {min: 2, max: 30})) {
        errors.login = 'Name must be between 2 and 30 characters';
    }

    if (Validator.isEmpty(data.login)) {
        errors.login = 'Name field is required';
    }

    if (Validator.isEmpty(data.email)) {
        errors.email = 'Email field is required';
    }

    if (!Validator.isEmail(data.email)) {
        errors.email = 'Email is invalid';
    }

    if (Validator.isEmpty(data.password)) {
        errors.password = 'Password field is required';
    }

    if (!Validator.isLength(data.password, {min: 6, max: 30})) {
        errors.password = 'Password must be at least 6 characters';
    }



    return {
        errors,
        isValid: isEmpty(errors)
    };
};

