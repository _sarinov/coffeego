'use strict';

'use strict';


module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('CoffeeCupSizes', [{
      name: "Caramel",
      capacity: "100.0",
      createdAt: new Date(),
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('CoffeeCupSizes', null, {});

  }
};

