'use strict';


module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('OrderTypes', [{
      name: "Fast",
      minPrice: "100",
      maxPrice:"200",
      createdAt: new Date(),
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('OrderTypes', null, {});

  }
};

