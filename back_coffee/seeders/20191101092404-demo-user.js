'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

      return queryInterface.bulkInsert('Users', [{
        login: "qwerty",
        password: "$2a$10$EI8NzfWo3a4dss.5lN7bGucIbcbwSOXiRyKR2oLg4lZIDp9gglV8u",
        email: "qwerty@gmail.com",
        phone: "87015386913",
        createdAt: new Date(),
      }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});

  }
};

