'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('CoffeTypes', [{
      name: "Latte",
      description: "About Latte",
      createdAt: new Date(),
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('CoffeTypes', null, {});

  }
};

