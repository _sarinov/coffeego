'use strict';

'use strict';


module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('CoffeeSyrups', [{
      name: "Caramel",
      description: "Caramel desc",
      createdAt: new Date(),
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('CoffeeSyrups', null, {});

  }
};

