const express = require("express");
const userController = require("../../controllers/userController");
const userRouter = express.Router();
const passport = require('passport/lib');

userRouter.post("/register",
    userController.register);

userRouter.post("/login",
    userController.login);
//
userRouter.get("/getCompanyId",
    passport.authenticate('jwt', {session: false}),
    userController.getCompanyId);

module.exports = userRouter;