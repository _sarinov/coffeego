const express = require("express");
const getOrderController = require("../../controllers/getCoffeeInfoController");
const infoRouter = express.Router();
const passport = require('passport/lib');


infoRouter.get("/coffeeType",
    getOrderController.getCoffeeTypes);

infoRouter.get("/coffeeSyrups",
    getOrderController.getCoffeeSyrups);

infoRouter.get("/coffeeCupSize",
    getOrderController.getCoffeeCupTypes);

infoRouter.get("/orderType",
    getOrderController.getOrderType);




module.exports = infoRouter;