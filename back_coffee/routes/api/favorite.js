const express = require("express");
const favotiteController = require("../../controllers/favoriteOrderController");
const favoriteRouter = express.Router();
const passport = require('passport/lib');

favoriteRouter.post("/",
     passport.authenticate('jwt', {session: false}),
    favotiteController.addToFovarite);

favoriteRouter.get("/",
    passport.authenticate('jwt', {session: false}),
    favotiteController.getMyFavorites);


module.exports = favoriteRouter;

