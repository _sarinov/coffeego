const express = require("express");
const updateOrderController = require("../../controllers/updateOrderController");
const getOrderController = require("../../controllers/getOrderControllers");
const orderRouter = express.Router();
const passport = require('passport/lib');

orderRouter.get("/",
    getOrderController.getOrder);

orderRouter.post("/",
    passport.authenticate('jwt', {session: false}),
    updateOrderController.addOrder);


orderRouter.get("/getByUserId",
    passport.authenticate('jwt', {session: false}),
    getOrderController.getByUserId);
orderRouter.get("/getByCompanyId",
    passport.authenticate('jwt', {session: false}),
    getOrderController.getByCompanyId);

orderRouter.get("/getActiveOrderByUserId",
    passport.authenticate('jwt', {session: false}),
    getOrderController.getActiveOrderByUserId);

orderRouter.put("/updateStatusOfOrder",
        passport.authenticate('jwt', {session: false}),
        updateOrderController.updateStatusOfOrder);
orderRouter.put("/updateCompanyId",
        passport.authenticate('jwt', {session: false}),
        updateOrderController.updateCompanyId);


module.exports = orderRouter;