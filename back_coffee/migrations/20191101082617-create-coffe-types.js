'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CoffeTypes', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull:false,
        autoIncrement: true
      },
      name: {
        type: Sequelize.CHAR(30)
      },
      description: {
        type: Sequelize.STRING
      },
      minPrice: {
        type: Sequelize.DOUBLE
      },
      maxPrice: {
        type: Sequelize.DOUBLE
      },
      createdAt: {
        type: Sequelize.DATE(3),
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3)'),
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CoffeTypes');
  }
};