'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Orders', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            price: {
                type: Sequelize.DOUBLE
            },
            status: {
                type: Sequelize.CHAR(7)
            },
            milk: {
                type: Sequelize.CHAR(3)
            },
            adressOfOrder: {
                type: Sequelize.TEXT
            },
            sugarCount: {
                type: Sequelize.INTEGER
            },
            userId: // name of the key we're adding
                {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'Users', // name of Target model
                        key: 'id', // key in Target model that we're referencing
                    }
                },
            coffeeTypeId: // name of the key we're adding
                {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'CoffeTypes', // name of Target model
                        key: 'id', // key in Target model that we're referencing
                    }
                },
            orderTypeId: // name of the key we're adding
                {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'OrderTypes', // name of Target model
                        key: 'id', // key in Target model that we're referencing
                    }
                },
            syrupId: // name of the key we're adding
                {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'CoffeeSyrups', // name of Target model
                        key: 'id', // key in Target model that we're referencing
                    }
                },
            sizeId: // name of the key we're adding
                {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'CoffeeCupSizes', // name of Target model
                        key: 'id', // key in Target model that we're referencing
                    }
                },
            companyId: // name of the key we're adding
                {
                    defaultValue: '0',
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'Companies', // name of Target model
                        key: 'id', // key in Target model that we're referencing
                    }
                },
            createdAt: {
                type: Sequelize.DATE(3),
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3)'),
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Orders');
    }
};