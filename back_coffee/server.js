const express  = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const ip = require('ip');
const path = require('path');

const cors = require('cors');

const user = require('./routes/api/user');
const order = require('./routes/api/order');
const favorite = require('./routes/api/favorite');
const orderInfo = require('./routes/api/orderInfo');

const app = express();
app.use(cors());

app.use(express.static(__dirname + '/uploads'));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const db = require('./models/index');

db.sequelize.sync({logging: false}).then(() => {
    console.log('Drop and Resync with { force: true }');
});

app.use(passport.initialize());

require('./config/passport')(passport);


app.get('/', (req, res) => res.send('Hello World!'));


app.use('/api/user', user);
app.use('/api/order', order);
app.use('/api/favorite', favorite);
app.use('/api/orderInfo', orderInfo);
//app.use('/api/test', test);


const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on port ${port}`));