const sequelize = require('sequelize');
const db = require('../models');


exports.addToFovarite = async function (req, res) {
    try {
        let fovarite = {
            userId: req.user.id,
            orderId: req.body.orderId,
        };
        db.Favorites.findOne({where: fovarite}).then(isExist => {
            if (!isExist)
                db.Favorites.create(fovarite).then(fovarited => {
                    res.json(fovarited)
                });
            else
                    res.json({msg:"Added Before"})

        })

    } catch (e) {
        res.json(e.message)
    }
};


exports.getMyFavorites= async function (req, res) {
    try {

        db.Orders.findAll({
            where: {
                userId: req.user.id
            },
            attributes:['price', 'status'],
            include: [{
                model: db.Users,
                attributes: ['login']
            },
                {
                    model: db.CoffeTypes,
                    attributes: ['name']

                },
                {
                    model: db.OrderType,
                    attributes: ['name']
                },
                {
                    model: db.Companies,
                    attributes: ['name']
                },
                {
                    model: db.Favorites,
                    attributes:[],
                    where:{userId: req.user.id}
                }
                ]


        }).then(oserorder => {
            res.json(oserorder)
        })

    } catch (e) {
        res.json(e.message)
    }
};


