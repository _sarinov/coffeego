const sequelize = require('sequelize');
const db = require('../models');

exports.getOrder = async function (req, res) {
    try {
        db.Orders.findAll({
            attributes:['id','price','status'],
            include: [{
                model: db.Users,
                attributes: ['login']
            },
                {
                    model: db.CoffeTypes,
                    attributes: ['name']

                },
                {
                    model: db.OrderType,
                    attributes: ['name']
                },
                {
                    model: db.Companies,
                    attributes: ['name']
                }]
        }).then(oreders => {
            res.json(oreders)
        })
    } catch (e) {
        res.json(e.message)
    }
};
exports.getByUserId = async function (req, res) {
    try {

        db.Orders.findAll({
            where: {
                userId: req.user.id
            },
            required:true,
            attributes:['id','price','status'],
            include: [{
                model: db.Users,
                attributes: ['login']
            },
                {
                    model: db.CoffeTypes,
                    attributes: ['name']

                },
                {
                    model: db.OrderType,
                    attributes: ['name']
                },
                {
                    model: db.Companies,
                    attributes: ['name']
                }]


        }).then(oserorder => {
            res.json(oserorder)
        })

    } catch (e) {
        res.json(e.message)
    }
};
exports.getByCompanyId = async function (req, res) {
    try {

        db.Orders.findAll({
            attributes:['price','status'],
            include: [{
                model: db.Users,
                attributes: ['login']
            },
                {
                    model: db.CoffeTypes,
                    attributes: ['name']

                },
                {
                    model: db.OrderType,
                    attributes: ['name']
                },
                {
                    model: db.Companies,
                    where:{
                      userId: req.user.id
                    },
                    attributes: ['name']
                }]


        }).then(oserorder => {
            res.json(oserorder)
        })

    } catch (e) {
        res.json(e.message)
    }
};
exports.getActiveOrderByUserId = async function (req, res) {
    try {
        db.Orders.findAll({
            where: {
                userId: req.user.id,
                status: 'active'
            },
            attributes:['price', 'status'],
            include: [{
                model: db.Users,
                attributes: ['login']
            },
                {
                    model: db.CoffeTypes,
                    attributes: ['name']

                },
                {
                    model: db.OrderType,
                    attributes: ['name']
                },
                {
                    model: db.Companies,
                    attributes: ['name']
                }]


        }).then(oserorder => {
            res.json(oserorder)
        })

    } catch (e) {
        res.json(e.message)
    }
};