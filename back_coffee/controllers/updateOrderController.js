const sequelize = require('sequelize');
const db = require('../models');


exports.addOrder = async function (req, res) {
    try {
        let order = {
            price: req.body.price,
            userId: req.user.id,
            coffeeTypeId: req.body.coffeeTypeId,
            orderTypeId: req.body.orderTypeId,
            syrupId: req.body.syrupId,
            sizeId: req.body.sizeId,
            companyId: 0,
            milk: req.body.milk,
            sugarCount:req.body.sugarCount,
            adressOfOrder: req.body.adressOfOrder,
            status: "active"
        };
        console.log(order.companyId);
        db.Orders.create(order).then(ordered => {
            res.json(ordered)
        })
    } catch (e) {
        res.json(e.message)
    }
};


exports.updateStatusOfOrder = async function (req, res) {
    try {
        let status;
        db.Orders.findByPk(req.body.id).then(order => {
            if (order.status === "active ")
                    status = "ready";
                 else if (order.status === "ready  " )
                    status = "active";

                db.Orders.update(
                    {
                        status: status
                    },
                    {
                        where: {
                            id: order.id
                        }
                    }).then(result => {
                        res.json({status: "success"})
                    }
                )
            }
        );

    } catch (e) {
        res.json(e.message)
    }
};

exports.updateCompanyId = async function (req, res) {
    try {
                db.Orders.update(
                    {
                        companyId: req.body.companyId
                    },
                    {
                        where: {
                            id: req.body.order
                        }
                    }).then(result => {
                        res.json({status: "success"})
                    }
                )
    } catch (e) {
        res.json(e.message)
    }
};

