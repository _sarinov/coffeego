const sequelize = require('sequelize');
const db = require('../models');
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../config/keys');


// Load input validation
const validateRegisterInput = require('../validation/register');
const validateLoginInput = require('../validation/login');
//const validateFriendInput = require('../validation/addfriend');


exports.register = async function (req, res) {
    const {errors, isValid} = validateRegisterInput(req.body);

    // Check Validation
    if (!isValid) {
        return res.status(400).json(errors);
    }

    db.Users.findOne({
        where: {login: req.body.login}
    })
        .then(user => {
            if (user) {
                return res.status(400).json({login: 'Login already exists'})
            } else {
                // const avatar = gravatar.url(req.body.email, {
                //     s: 200, // Size
                //     r: 'pg', //Rating
                //     d: 'mm' //Default
                // });

                const newUser = {
                    login: req.body.login,
                    email: req.body.email,
                    password: req.body.password,
                };

                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        if (err) throw err;
                        newUser.password = hash;
                        db.Users.create(newUser).then(user => {
                            res.json(user);
                        })
                            .catch(err => console.log(err))
                    })
                })
            }
        })  .catch(err => console.log(err))
};

exports.login = async function (req, res) {
    const {errors, isValid} = validateLoginInput(req.body);

    console.log(req.body.login);


    // Check Validation
    if (!isValid) {
        return res.status(400).json(errors);
    }

    const login = req.body.login;
    const password = req.body.password;


    // Find user by email
    db.Users.findOne({
        where: {login:login}
    })
        .then(user => {
            // Check for user
            if (!user) {
                errors.login = 'User not found';
                return res.status(404).json(errors);
            }

            // Check Password
            bcrypt.compare(password, user.password)
                .then(isMatch => {
                    if (isMatch) {
                        // User matched

                        // Create JWT Payload
                        const payload = {
                            id: user.id,
                            login: user.login,
                        };

                        // Sign token
                        jwt.sign(
                            payload,
                            keys.secretOrKey,
                            {expiresIn: 5465346},
                            (err, token) => {
                                res.json({
                                    success: true,
                                    id: user.id,
                                    token: token,
                                    role: user.role
                                });
                            }
                        );
                    } else {
                        errors.password = 'Password incorrect';
                        return res.status(400).json(errors);
                    }
                })  .catch(err => console.log(err))
        })  .catch(err => console.log(err))
};

exports.getCompanyId = async function (req, res) {
    db.Companies.findOne({
        where:{
            userId: req.user.id
        }
    }).then(result => {
        res.json(result)
    })
}
exports.current = async function (req, res) {
    res.json({
        id: req.user.id,
        name: req.user.name,
        login: req.user.login
    });
};
