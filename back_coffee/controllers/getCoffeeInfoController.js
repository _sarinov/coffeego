const sequelize = require('sequelize');
const db = require('../models');

exports.getCoffeeTypes = async function (req, res) {
    try {
        db.CoffeTypes.findAll().then(type => {
            res.json(type)
        })
    } catch (e) {
        res.json(e.message)
    }
};

exports.getCoffeeSyrups = async function (req, res) {
    try {
        db.CoffeeSyrups.findAll().then(type => {
            res.json(type)
        })
    } catch (e) {
        res.json(e.message)
    }
};

exports.getOrderType = async function (req, res) {
    try {
        db.OrderType.findAll().then(type => {
            res.json(type)
        })
    } catch (e) {
        res.json(e.message)
    }
};

exports.getCoffeeCupTypes = async function (req, res) {
    try {
        db.CoffeeCupSizes.findAll().then(type => {
            res.json(type)
        })
    } catch (e) {
        res.json(e.message)
    }
};

