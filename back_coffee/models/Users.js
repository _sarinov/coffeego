'use strict';
const DataTypeS = require('sequelize').DataTypes;

module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {
    login: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    role: DataTypes.STRING,
  }, {});
  Users.associate = function(models) {
    Users.hasMany(models.Orders, {
      foreignKey: 'userId',
      onDelete: 'CASCADE',
    });
    Users.hasMany(models.Favorites, {
      foreignKey: 'userId',
      onDelete: 'CASCADE',
    });
    Users.hasMany(models.Companies, {
      foreignKey: 'userId',
      onDelete: 'CASCADE',
    });
};

  return Users;
};