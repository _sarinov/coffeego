'use strict';
module.exports = (sequelize, DataTypes) => {
  const CoffeeSyrup = sequelize.define('CoffeeSyrups', {
    name: DataTypes.CHAR(30),
    description: DataTypes.CHAR(30),
  }, {});
  CoffeeSyrup.associate = function(models) {
    CoffeeSyrup.hasMany(models.Orders, {
      foreignKey: 'syrupId',
      onDelete: 'CASCADE',
    });
  };
  return CoffeeSyrup;
};