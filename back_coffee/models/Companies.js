'use strict';
module.exports = (sequelize, DataTypes) => {
  const Companies = sequelize.define('Companies', {
    name: DataTypes.CHAR(30),
    description: DataTypes.STRING
  }, {});
  Companies.associate = function(models) {
    Companies.hasMany(models.Orders, {
      foreignKey: 'companyId',
      onDelete: 'CASCADE',
    });
    Companies.belongsTo(models.Users, {
      foreignKey: 'userId',
      onDelete: 'CASCADE',
    });
  };
  return Companies;
};