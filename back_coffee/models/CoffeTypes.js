'use strict';
module.exports = (sequelize, DataTypes) => {
  const CoffeTypes = sequelize.define('CoffeTypes', {
    name: DataTypes.CHAR(30),
    description: DataTypes.STRING,
    minPrice: DataTypes.DOUBLE,
    maxPrice: DataTypes.DOUBLE
  }, {});
  CoffeTypes.associate = function(models) {
    CoffeTypes.hasMany(models.Orders, {
      foreignKey: 'coffeeTypeId',
      onDelete: 'CASCADE',
    });  };
  return CoffeTypes;
};