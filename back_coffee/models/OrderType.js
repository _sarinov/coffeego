'use strict';
module.exports = (sequelize, DataTypes) => {
    const OrderType = sequelize.define('OrderType', {
        name: DataTypes.CHAR(30),
        minPrice: DataTypes.DOUBLE,
        maxPrice: DataTypes.DOUBLE
    }, {});
    OrderType.associate = function (models) {
        OrderType.hasMany(models.Orders, {
            foreignKey: 'orderTypeId',
            onDelete: 'CASCADE',
        });
    };
    return OrderType;
};