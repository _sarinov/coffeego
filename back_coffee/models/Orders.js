'use strict';
module.exports = (sequelize, DataTypes) => {
  const Orders = sequelize.define('Orders', {
    price: DataTypes.DOUBLE,
    milk: DataTypes.CHAR(3),
    sugarCount: DataTypes.INTEGER,
    adressOfOrder: DataTypes.TEXT,
    status: DataTypes.CHAR
  }, {});
  Orders.associate = function(models) {
    Orders.belongsTo(models.CoffeTypes, {
      foreignKey: 'coffeeTypeId',
      onDelete: 'CASCADE',
    });
    Orders.belongsTo(models.Companies, {
      foreignKey: 'companyId',
      onDelete: 'CASCADE',
    });
    Orders.belongsTo(models.Users, {
      foreignKey: 'userId',
      onDelete: 'CASCADE',
    });
    Orders.belongsTo(models.CoffeeSyrups, {
      foreignKey: 'syrupId',
      onDelete: 'CASCADE',
    });
    Orders.belongsTo(models.CoffeeCupSizes, {
      foreignKey: 'sizeId',
      onDelete: 'CASCADE',
    });
    Orders.belongsTo(models.OrderType, {
      foreignKey: 'orderTypeId',
      onDelete: 'CASCADE',
    });
    Orders.hasMany(models.Favorites, {
      foreignKey: 'orderId',
      onDelete: 'CASCADE',
    });


  };
  return Orders;
};