'use strict';
module.exports = (sequelize, DataTypes) => {
  const Favorites = sequelize.define('Favorites', {
    userId: DataTypes.INTEGER,
    orderId: DataTypes.INTEGER
  }, {});
  Favorites.associate = function(models) {
    Favorites.belongsTo(models.Orders, {
      foreignKey: 'orderId',
      onDelete: 'CASCADE',
    });
    Favorites.belongsTo(models.Users, {
      foreignKey: 'userId',
      onDelete: 'CASCADE',
    });
  };
  return Favorites;
};