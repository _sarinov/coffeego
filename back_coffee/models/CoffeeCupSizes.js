'use strict';
module.exports = (sequelize, DataTypes) => {
  const CoffeeCupSizes = sequelize.define('CoffeeCupSizes', {
    name: DataTypes.CHAR(30),
    capacity: DataTypes.DOUBLE
  }, {});
  CoffeeCupSizes.associate = function(models) {
    CoffeeCupSizes.hasMany(models.Orders, {
      foreignKey: 'sizeId',
      onDelete: 'CASCADE',
    });
  };
  return CoffeeCupSizes;
};