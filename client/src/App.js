import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Provider} from 'react-redux';
import { BrowserRouter as Router, Route, withRouter, Switch} from 'react-router-dom';
import  store from './store';
import Header from "./components/header";
import 'antd/dist/antd.css';
import Main from "./containers/main";
import setAuthToken from "./utils/setAuthToken";
import {logoutUser, setCurrentUser} from "./actions/authActions";
import jwt_decode from 'jwt-decode'
import RouteCompany from "./components/Route/Company";
import RouteUser from "./components/Route";
import Company from "./containers/company";
import UserOrder from "./containers/order";
import Favorites from "./containers/favorites";


if(localStorage.jwtToken){
  setAuthToken(localStorage.jwtToken);
  const decoded=jwt_decode(localStorage.jwtToken);
  store.dispatch(setCurrentUser(decoded));
  const currentTime=Date.now()/1000;
  if(decoded.exp<currentTime){
    store.dispatch(logoutUser());
    // store.dispatch(clearCurrentProfile());
    window.location.href='/';
  }
}
function App() {

  return (
      <Provider store={store}>
        <Router>
    <div className="App">
      <Route exact path={'/'} component={Main} />
      <Route exact path={'/orders'} component={UserOrder} />
      <Route exact path={'/favorites'} component={Favorites} />
    </div>
        </Router>
      </Provider>
  );
}


export default App;
