import  {combineReducers} from "redux";
import authReducer from './authReducer';
import modalReducer from "./modalReducer";
import  coffeeReducer from "./coffeeReducer"
export default combineReducers({
    coffee:coffeeReducer,
    auth: authReducer,
    modal:modalReducer,
});
