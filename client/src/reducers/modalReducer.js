import {MODAL_STATE} from "../actions/types";

const initialState={
    modal:false
};

export default function (state=initialState,action) {

    switch (action.type) {
        case MODAL_STATE:
            return {
                modal:action.payload
            };
        default:
            return state;
    }
}