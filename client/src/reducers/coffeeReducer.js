import {
    GET_COFFEES,
    GET_DELIVERY,
    GET_FAVORITES,
    GET_ORDER_USER,
    GET_ORDERS,
    GET_SIZES,
    GET_SYRUPS
} from "../actions/types";

const initialState={
    coffee:[],
    syrup:[],
    size:[],
    delivery:[],
    orders:[],
    userOrders:[],
    favorites:[],

}
export default function (state=initialState,action) {
    switch (action.type) {
        case GET_COFFEES:
            return {
                ...state,
                coffee: action.payload,
            }
        case GET_ORDERS:
            return {
                ...state,
                orders: action.payload,
            }
        case GET_ORDER_USER:
            return {
                ...state,
                userOrders:action.payload
            }
        case GET_SYRUPS:
            return {
                ...state,
                syrup: action.payload,
            }
        case GET_FAVORITES:
            return {
                ...state,
                favorites: action.payload,
            }
        case GET_SIZES:
            return {
                ...state,
                size: action.payload,
            }
        case GET_DELIVERY:
            return {
                ...state,
                delivery: action.payload,
            }
        default:
            return state;
    }
}
