 export const IP="http://localhost:5000";
export const GET_ERRORS='GET_ERRORS';
export const SET_CURRENT_USER='SET_CURRENT_USER';
 export const MODAL_STATE='MODAL_STATE';
 export const GET_COFFEES='GET_COFFEES';
 export const GET_SYRUPS='GET_SYRUPS';
 export const GET_SIZES='GET_SIZES';
 export const GET_DELIVERY='GET_DELIVERY';
 export const GET_ORDERS='GET_ORDERS';
 export const GET_ORDER_USER='GET_ORDER_USER';
 export const GET_FAVORITES='GET_FAVORITES';
 export const GET_COMPANY = 'GET_COMPANY'


