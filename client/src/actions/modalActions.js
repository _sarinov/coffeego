
import {MODAL_STATE} from './types'

export const modalClose=dispatch=>{
    return {
        type: MODAL_STATE,
        payload: false
    }
};

export const modalOpen=dispatch=>{
    return {
        type: MODAL_STATE,
        payload: true
    }
};
