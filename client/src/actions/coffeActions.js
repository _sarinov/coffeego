import axios from "axios";

import {
    GET_COFFEES,
    GET_DELIVERY,
    GET_ERRORS, GET_FAVORITES,
    GET_ORDER_USER,
    GET_ORDERS,
    GET_SIZES,
    GET_SYRUPS,
    IP,
    MODAL_STATE
} from "./types";
import { Modal, Button } from 'antd';
export const saveOrder=(userData,history)=>dispatch=>{
    axios
        .post(IP+'/api/order',userData)
        .then(res=> {

            Modal.success({
                content: 'You have ordered',
            });


                return dispatch ({
                    type: MODAL_STATE,
                    payload: false
                })
            }

        )
        .catch(err=> {
                return dispatch({
                    type: GET_ERRORS,
                    payload: err
                });
            }
        );
};
export const getSyrups=()=>dispatch=>{
    axios
        .get(IP+'/api/orderInfo/coffeeSyrups')
        .then(res=> {
                return dispatch ({
                    type: GET_SYRUPS,
                    payload: res.data
                })
            }

        )
        .catch(err=> {
                return dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                });
            }
        );
};
export const getCoffes=()=>dispatch=>{
    axios
        .get(IP+'/api/orderInfo/coffeeType')
        .then(res=> {
                return dispatch ({
                    type: GET_COFFEES,
                    payload: res.data
                })
            }

        )
        .catch(err=> {
                return dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                });
            }
        );
};
export const getSizes=()=>dispatch=>{
    axios
        .get(IP+'/api/orderInfo/coffeeCupSize')
        .then(res=> {
                return dispatch ({
                    type: GET_SIZES,
                    payload: res.data
                })
            }
        )
        .catch(err=> {
                return dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                });
            }
        );
};
export const getDelivery=()=>dispatch=>{
    axios
        .get(IP+'/api/orderInfo/orderType')
        .then(res=> {
                return dispatch ({
                    type: GET_DELIVERY,
                    payload: res.data
                })
            }
        )
        .catch(err=> {
                return dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                });
            }
        );
};

export const getOrders=()=>dispatch=>{
axios
    .get(IP+'/api/order')
    .then(res=> {
            return dispatch ({
                type: GET_ORDERS,
                payload: res.data
            })
        }
    )
    .catch(err=> {
            return dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            });
        }
    );
}
export const getActiveOrders=()=>dispatch=>{
    axios
        .get(IP+'/api/order/getActiveOrderByUserId')
        .then(res=> {
                return dispatch ({
                    type: GET_ORDERS,
                    payload: res.data
                })
            }
        )
        .catch(err=> {
                return dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                });
            }
        );
}

export const getByUser=()=>dispatch=>{
    axios
        .get(IP+'/api/order/getByUserId')
        .then(res=> {
                return dispatch ({
                    type: GET_ORDER_USER,
                    payload: res.data
                })
            }
        )
        .catch(err=> {
                return dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                });
            }
        );
}
export const getFavorites=()=>dispatch=>{
    axios
        .get(IP+'/api/favorite')
        .then(res=> {
                return dispatch ({
                    type: GET_FAVORITES,
                    payload: res.data
                })
            }
        )
        .catch(err=> {
                return dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                });
            }
        );
}
export const addFavorite=(id)=>dispatch=>{
    axios
        .post(IP+'/api/favorite',{orderId:id})
        .then(res=> {

            }
        )
        .catch(err=> {
                return dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                });
            }
        );
}
export const takeOrder=(id)=>dispatch=>{
    console.log(id)
    axios
        .put(IP+'/api/order/updateCompanyId',{id:id})
        .then(res=> {
                Modal.success({
                    content: 'Preparing...',
                });
            }
        )
        .catch(err=> {
                return dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                });
            }
        );
}
export const updateStatus=(id)=>dispatch=>{
    axios
        .put(IP+'/api/order/updateStatusOfOrder',{id:id})
        .then(res=> {
            dispatch(getByUser());
            }
        )
        .catch(err=> {
                return dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                });
            }
        );
}