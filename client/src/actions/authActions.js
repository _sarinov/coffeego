
import {GET_ERRORS, MODAL_STATE, SET_CURRENT_USER, IP, GET_FAVORITES, GET_COMPANY} from './types'
import setAuthToken from '../utils/setAuthToken'
import axios from 'axios'
import jwt_decode from 'jwt-decode'
export const registerUser=(userData,history)=>dispatch=>{
    axios
        .post(IP+'/api/user/register',userData)
        .then(res=> {

            alert("You have registered");

                history.push('/');

                return dispatch ({
                    type: MODAL_STATE,
                    payload: false

                })


            }

        )
        .catch(err=> {
                return dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                });
            }
        );
};


export const loginUser=(userData,history)=>dispatch=>{
    axios.post(IP+'/api/user/login',userData)
        .then(res=>{
            const {token} =res.data;
            localStorage.setItem('jwtToken', token);
            localStorage.setItem('role',res.data.role)
            setAuthToken(token);

            let decoded={
                token:jwt_decode(token),
                role:res.data.role
            };
            console.log(decoded);
            dispatch(setCurrentUser(decoded));
                history.push('/');

            return dispatch ({
                type: MODAL_STATE,
                payload: false
            })


        })
        .catch(err=>{
            return dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            });
        })
}

export const clearErrors=()=>dispatch=>{
    return dispatch({
        type: GET_ERRORS,
        payload: {}
    });
}

export const setCurrentUser=(decoded)=>{
    return{
        type:SET_CURRENT_USER,
        payload:decoded
    }
}

export const logoutUser=(history)=>dispatch=>{
    localStorage.removeItem('jwtToken');
    localStorage.removeItem('role');
    setAuthToken(false);
    dispatch(setCurrentUser({}));
    history.push('/');
}
export const getCompany=()=>dispatch=>{
    axios
        .get(IP+'/api/user/getCompanyId')
        .then(res=> {
                return dispatch ({
                    type: GET_COMPANY,
                    payload: res.data
                })
            }
        )
        .catch(err=> {
                return dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                });
            }
        );
}