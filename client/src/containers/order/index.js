
import React, {Component} from 'react';

import {Button, Form, Input,InputNumber} from "antd";
import Header from "../../components/header";
import Modal from "antd/es/modal";
import {Card,Row,Col} from 'antd'
import Select from "antd/es/select";
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux'
import {addFavorite, getActiveOrders, getByUser, getOrders, takeOrder, updateStatus} from "../../actions/coffeActions";
import Icon from "antd/es/icon";
import {getCompany} from "../../actions/authActions";
import axios from "axios"
import {    IP} from "../../actions/types";
const {Option}=Select;

class UserOrder extends Component{

    constructor(){
        super();
        this.state={
            visible:false,
            heart:"outlined"
        }
    }


    handleFavorite=(id)=>{
        console.log(id)
        axios
            .post(IP+'/api/favorite',{orderId:id})
            .then(res=> {
                this.setState({
                    heart:"filled"
                })
                }
            )
            .catch(err=> {
                  console.log(err)
                }
            );





    }
    handleTake=()=>{
         if(this.props.auth && this.props.auth.company && this.props.auth.company.id ) {
             this.props.takeOrder(this.props.auth.company.id);
         }
    }
    handleStatus=(id)=>{
        this.props.updateStatus(id);

    }
    componentDidMount() {
        this.props.getByUser();
        this.props.getCompany();
    }

    render(){
        console.log(this.props.coffee)
        const {heart}=this.state;
        const {coffee}=this.props;
        let order;
        if (localStorage.getItem('role')==='company'){
            order = coffee.userOrders.map((item,i)=>(
                <Col  key={i} span={24}>
                    <Card  title={`Order ${i+1}`} extra={<span style={{cursor:"pointer"}}  onClick={()=>this.handleTake(item.id)}>Take</span>} style={{ width: '50%',margin:"0 auto",border:"1px solid #c0aa83"}}>
                        <p>Price: {item.price}tg</p>
                        <p>Сoffee Type: {item.CoffeType.name}</p>
                        <p>Order type: {item.OrderType.name}</p>
                        <p>Status: {item.status}</p>


                    </Card>
                </Col>
            ))
        }
        else{
            order = coffee.userOrders.map((item,i)=>(
                <Col  key={i} span={24}>
                    <Card  title={`Order ${i+1}`} extra={<span style={{cursor:"pointer"}} ><Icon onClick={()=>this.handleFavorite(item.id)} type="heart" theme={heart} /><span onClick={()=>this.handleStatus(item.id)}  style={{paddingLeft:"20px"}}>Update status</span></span>} style={{ width: '50%',margin:"0 auto",border:"1px solid #c0aa83"}}>
                        <p>Price: {item.price}tg</p>
                        <p>Сoffee Type: {item.CoffeType.name}</p>
                        <p>Order type: {item.OrderType.name}</p>
                        <p>Status: {item.status}</p>


                    </Card>
                </Col>
            ))
        }


        return(
            <div className="mainClass">
                <Header/>
                <div className="order">
                    <h1 style={{textAlign:"center",fontSize:"72px",color:"#fff"}}>My Orders</h1>
                </div>

                <Row  gutter={[16,16]}>
                    {order}
                </Row>

            </div>
        )
    }
}
const mapStateToProps=(state)=>({
    coffee:state.coffee,
    modal:state.modal,
    auth:state.auth,
})
export default connect(mapStateToProps,{takeOrder,updateStatus,getCompany,getActiveOrders,getByUser,addFavorite}) (withRouter(UserOrder));