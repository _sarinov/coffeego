

import React, {Component} from 'react';

import {Button, Col, Form, Input, InputNumber, Row} from "antd";
import Header from "../../components/header";
import Modal from "antd/es/modal";
import {Card} from 'antd'
import Select from "antd/es/select";
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux'
import {getFavorites, getOrders} from "../../actions/coffeActions";

const {Option}=Select;



class Favorites extends Component{

    constructor(){
        super();
        this.state={
            visible:false,

        }
    }


    handleFavorite=(id)=>{

    }
    componentDidMount() {
        this.props.getFavorites();
    }

    render(){
        console.log(this.props.coffee)
        const {coffee}=this.props;
        let order = coffee.favorites.map((item,i)=>(
            <Col  key={i} span={24}>
                <Card  title="" extra={<span style={{cursor:"pointer"}} onClick={()=>this.handleFavorite(item.id)}>Remove from favorites</span>} style={{ width: '50%',margin:"0 auto",border:"1px solid #c0aa83"}}>
                    <p>Price: {item.price}tg</p>
                    <p>Сoffee Type: {item.CoffeType.name}</p>
                    <p>Order type: {item.OrderType.name}</p>
                    <p>Status: {item.status}</p>

                </Card>
            </Col>
        ))

        return(
            <div className="mainClass">
                <Header/>
                <div className="favorite">
                    <h1 style={{textAlign:"center",fontSize:"72px",color:"#fff"}}>Favorites</h1>
                </div>
                <Row  gutter={[16,16]}>
                    {order}
                </Row>



            </div>
        )
    }
}
const mapStateToProps=(state)=>({
    coffee:state.coffee,
    modal:state.modal,

})
export default connect(mapStateToProps,{getFavorites}) (withRouter(Favorites));