
import React, {Component} from 'react';

import {Button, Form, Input,InputNumber, Card} from "antd";
import Header from "../../components/header";
import Modal from "antd/es/modal";
import Select from "antd/es/select";
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux'
import {getOrders} from "../../actions/coffeActions";

const {Option}=Select;



class Company extends Component{

    constructor(){
        super();
        this.state={
            visible:false,

        }
    }
    componentDidMount() {
        this.props.getOrders();
    }

    render(){
        console.log(this.props.coffee)
        const {coffee}=this.props;
        let order = coffee.orders.map((item,i)=>(
            <Card title={item.CoffeType.name} extra={<a href="#">More</a>} style={{ width: 300 }} key = {i}>
                <p>{item.price}</p>
                <p>{item.milk}</p>
                <p>{item.status}</p>

            </Card>
        ))

        return(

            <div className="mainClass">
                <Header/>
                <div className="main">
                    <h1 style={{textAlign:"center",fontSize:"72px",color:"#fff"}}>Orders</h1>
                </div>
                <div style={{width:"100%"}}>
                    {order}
                </div>


            </div>
        )
    }
}
const mapStateToProps=(state)=>({
    coffee:state.coffee,
    modal:state.modal,

})
export default connect(mapStateToProps,{getOrders}) (withRouter(Company));