import React from 'react';
import {withRouter} from 'react-router-dom';

import PropTypes from 'prop-types';
import {
    Form,
    Input,
    Select,
    Button,
    AutoComplete,
} from 'antd';
import {connect} from "react-redux";
import {registerUser, clearErrors} from "../../../actions/authActions";





class RegistrationForm extends React.Component {
    state = {
        isRequestSend: false,
        confirmDirty: false,
        autoCompleteResult: [],
        errors:{}
    };

    componentDidMount() {
        this.setState({isRequestSend: false});
        this.props.clearErrors();
        console.log(2);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.errors)
            this.setState({errors: nextProps.errors});

        console.log(nextProps.errors, 1)
    }


    handleSubmit = e => {
        e.preventDefault();



        this.props.form.validateFieldsAndScroll((err, values) => {

            if (!err) {
                console.log('Received values of form: ', values);
              this.props.form.resetFields(['email','login','password','password2'])
                this.props.registerUser(values,this.props.history);


            }
        });

    };



    handleConfirmBlur = e => {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    };

    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('Пароли не одинаковы');
        } else {
            callback();
        }
    };

    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], { force: true });
        }
        callback();
    };



    render() {
        const {errors} = this.state;
        const {user}=this.props.auth;

        const { getFieldDecorator } = this.props.form;


        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };

        return (
            <div className="form-register">

                <div className="container-landing">
                    <h3>Создайте аккаунт</h3>
                    <div className="form-register--inner">

                        <Form   {...formItemLayout} onSubmit={this.handleSubmit}>
                            <Form.Item
                                label={
                                    <span>
                              Nickname&nbsp;</span>
                                }
                            >
                                {getFieldDecorator('login', {
                                    rules: [{ required: true,
                                        message: 'Name must be between 2 and 33 symbols!',
                                        min:2,
                                        max:33,
                                        whitespace: true }],
                                })(<Input />)}
                                {errors.name}
                            </Form.Item>
                            <Form.Item label="E-mail">
                                {getFieldDecorator('email', {
                                    rules: [
                                        {
                                            type: 'email',
                                            message: 'Неправильный e-mail!',

                                        },
                                        {
                                            required: true,
                                            message: 'Пожалуйста введите e-mail!',
                                        },
                                    ],
                                })(<Input />)}
                                {errors.email}
                            </Form.Item>
                            <Form.Item label="Пароль" hasFeedback>
                                {getFieldDecorator('password', {
                                    rules: [
                                        {
                                            required: true,
                                            message: 'Длина пароля не должна быть меньше 6 символов',
                                            min:6
                                        },
                                        {
                                            validator: this.validateToNextPassword,
                                        },
                                    ],
                                })(<Input.Password />)}
                                {errors.password}
                            </Form.Item>
                            <Form.Item label="Введите еще раз" hasFeedback>
                                {getFieldDecorator('password2', {
                                    rules: [
                                        {
                                            required: true,
                                            message: '',
                                            min:6
                                        },
                                        {
                                            validator: this.compareToFirstPassword,
                                        },
                                    ],
                                })(<Input.Password onBlur={this.handleConfirmBlur} />)}

                            </Form.Item>





                            <Form.Item {...tailFormItemLayout}>
                                <Button  type="primary" style={{background:"#6f625b",borderColor:"#6f625b"}} htmlType="submit">
                                    Register
                                </Button>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </div>
        );
    }
}



const WrappedRegistrationForm = Form.create({ name: 'register' })(RegistrationForm);


const mapStateToProps=(state)=>({
    auth:state.auth,
    errors:state.errors,

})

export default connect(mapStateToProps,{registerUser, clearErrors}) (withRouter(WrappedRegistrationForm));
