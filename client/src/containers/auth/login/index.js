import React, { Component } from 'react'
import {withRouter} from 'react-router-dom';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import {connect} from 'react-redux';
import {loginUser} from "../../../actions/authActions";
import PropTypes from "prop-types";
import Modal from "antd/es/modal";


class Login extends Component {
    constructor(){
        super();
        this.state={
            visible:false,
        }
}

    handleSubmit = e => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err) {

                console.log('Received values of form: ', values);
               this.props.form.resetFields(['login','password'])
                this.props.loginUser(values,this.props.history)
            }
        });
    };


    render (){

        const {errors}=this.props;
        const { getFieldDecorator } = this.props.form;

        return(
            <div className="form-register">
                <div className="container-landing">
                    <h3>Войти в аккаунт</h3>
                    <div className="form-login--inner">
                        <Form onSubmit={this.handleSubmit} className="login-form" name='login'>
                            <Form.Item>
                                {getFieldDecorator('login', {
                                    rules: [
                                        {
                                            required:true,
                                            message: 'Неправильный e-mail!',

                                        },

                                    ],
                                })(
                                    <Input
                                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        placeholder="Login"
                                    />,
                                )}

                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Пожалуйста введите пароль!',
                                    }],
                                })(
                                    <Input
                                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        type="password"
                                        placeholder="Password"
                                    />,
                                )}

                            </Form.Item>
                            <Form.Item>

                                <Button type="primary"  style={{background:"#6f625b",borderColor:"#6f625b"}} htmlType="submit" className="login-form-button">
                                    Log in
                                </Button>

                            </Form.Item>
                        </Form>
                    </div>
                </div>

            </div>
        )

    }


}




const mapStateToProps=(state)=>({
    auth:state.auth,
    errors:state.errors,

})
const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Login);
export default connect(mapStateToProps,{loginUser})(withRouter(WrappedNormalLoginForm));
