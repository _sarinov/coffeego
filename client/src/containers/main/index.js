import React, {Component} from 'react';
import './main.css';
import {Button, Form, Input,InputNumber} from "antd";
import Header from "../../components/header";
import Modal from "antd/es/modal";
import Select from "antd/es/select";
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux'
import FormItem from "antd/es/form/FormItem";
import {getCoffes, getDelivery, getSizes, getSyrups, saveOrder} from "../../actions/coffeActions";

import { YMaps, Map, Placemark } from "react-yandex-maps";
import RadioGroup from "antd/es/radio/group";
import {getCompany} from "../../actions/authActions";
const {Option}=Select;



function onBlur() {

}

function onFocus() {
}

function onSearch(val) {
}

class Main extends Component{

    constructor(){
        super();
        this.state={
            visible:false,
            sugar:0,
            price:0,
            milk:'',
            size:0,
            delivery:0,
            coffee:0,
            syrup:0,
            coords: '',
            adressOfOrder:'',
        }
    }

    handleOk=()=>{
        let data ={
            price: this.state.price,
            coffeeTypeId: this.state.coffee,
            orderTypeId: this.state.delivery,
            syrupId: this.state.syrup,
            sizeId: this.state.size,
            companyId: 0,
            milk:this.state.milk,
            size:this.state.size,
            syrup:this.state.syrup,
            delivery:this.state.delivery,
            coffee:this.state.coffee,
            adressOfOrder:this.state.adressOfOrder,
        }
        this.props.saveOrder(data,this.props.history);
        this.setState({
            visible:this.props.modal.modal,
            sugar:0,
            price:'',
            milk:'',
            size:'',
            delivery:'',
            coffee:'',
            syrup:'',
            adressOfOrder:'',
        })

    }
    handleCancel=()=>{
        this.setState({
            visible:false,
        })
    }
    openModal=()=>{
        this.setState({
            visible:true,
        })
    }


    onChangeCoffee(value) {
        console.log(`selected ${value}`);
        this.setState({
            coffee:value
        })
    }
    onChangeSyrup(value) {
        console.log(`selected ${value}`);
        this.setState({
            syrup:value
        })
    }

    onChangeDelivery(value) {
        console.log(`selected ${value}`);
        this.setState({
            delivery:value
        })
    }

    onChangeMilk(value) {
        console.log(`selected ${value}`);
        this.setState({
            milk:value
        })
    }

    onChangeSize(value) {
        console.log(`selected ${value}`);
        this.setState({
            size:value
        })
    }




    componentDidMount() {
            this.props.getCoffes();
             this.props.getSyrups();
        this.props.getSizes();
        this.props.getDelivery();
        this.props.getCompany();


    }
     onChangeCoffee=(value)=> {
        console.log(`selected ${value}`);
        this.setState({
            coffee:value
        })
    }
    onChangeSyrup=(value)=> {
        console.log(`selected ${value}`);
        this.setState({
            syrup:value
        })
    }

     onChangeDelivery=(value)=> {
        console.log(`selected ${value}`);
        this.setState({
            delivery:value
        })
    }

    onChangeMilk=(e)=> {
        console.log(`selected ${e.target.value}`);
        this.setState({
            milk:e.target.value
        })
    }

     onChangeSize=(value)=> {
        console.log(`selected ${value}`);
        this.setState({
            size:value
        })
    }
    onChange=(e)=>{
        this.setState({
           [e.target.name]:e.target.value
        })
    }

    onMapClick=(event)=> {
        this.setState(state => {
            return {
                coords: event.get("coords")
            };
        });
    }
    onChangeSugar=(value)=>{
        this.setState({
            sugar:value
        })
    }
    render(){
    const {visible,sugar,price,adressOfOrder}=this.state;
    const {coffee}=this.props;
    console.log(this.props.auth)
        let milk=['yes','no']
    let coffees=[],syrups=[],sizes=[],delivery=[];

    if(coffee && coffee.coffee && coffee.coffee.length) {
        coffees = coffee.coffee.map((item, i) => (
            <Option value={item.id} key={i}>{item.name}</Option>
        ))
    }
        if(coffee && coffee.syrup && coffee.syrup.length) {
            syrups = coffee.syrup.map((item, i) => (
                <Option value={item.id} key={i}>{item.name}</Option>
            ))
        }
            if(coffee && coffee.size && coffee.size.length) {
                sizes = coffee.size.map((item, i) => (
                    <Option value={item.id} key={i}>{item.name}</Option>
                ))
            }
                if(coffee && coffee.delivery && coffee.delivery.length) {
                    delivery = coffee.delivery.map((item, i) => (
                        <Option value={item.id} key={i}>{item.name}</Option>
                    ))
                }
    const {auth}=this.props;
        return(
            <div className="mainClass">
                <Header/>
            <div className="main">
                <div className="back-img">
                    <img src="/assets/background.png"/>
                </div>
                {localStorage.getItem('role')==='company' ? "" :  <Button type="primary" onClick={this.openModal} style={{background:" #c0aa83",border:"none",borderRadius:0,fontSize:"18px",fontWeight:700,color:"#fff",width:"180px",height:"40px"}}>Order</Button> }
            </div>
                <Modal
                    visible={visible}
                    title="Заказ"
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="edit" onClick={this.handleOk}>
                            Сохранить
                        </Button>,
                        <Button key="back" onClick={this.handleCancel}>
                            Отменить
                        </Button>,

                    ]}
                >

                    <Form layout={"vertical"}>
                        <Form.Item label="Coffee">
                            <Select
                                showSearch
                                style={{ width: '100%' }}
                                placeholder="Choose coffee"
                                optionFilterProp="children"
                                onChange={this.onChangeCoffee}
                                onFocus={onFocus}
                                onBlur={onBlur}
                                onSearch={onSearch}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {coffees}
                            </Select>
                        </Form.Item>
                        <Form.Item label="Syrup">
                            <Select
                                showSearch
                                style={{ width: '100%' }}
                                placeholder="Choose Syrup"
                                optionFilterProp="children"
                                onChange={this.onChangeSyrup}
                                onFocus={onFocus}
                                onBlur={onBlur}
                                onSearch={onSearch}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {syrups}
                            </Select>
                        </Form.Item>
                        <Form.Item label="Milk">
                            <RadioGroup onChange={this.onChangeMilk} options={milk}/>
                        </Form.Item>
                        <Form.Item label="Sugar amount ">
                            <InputNumber style={{width:'100%'}}  min={0}  max={10} defaultValue={0} onChange={this.onChangeSugar} />
                        </Form.Item>
                        <Form.Item label="Size">
                            <Select
                                showSearch
                                style={{ width: '100%' }}

                                optionFilterProp="children"
                                onChange={this.onChangeSize}
                                onFocus={onFocus}
                                onBlur={onBlur}
                                onSearch={onSearch}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                               {sizes}
                            </Select>
                        </Form.Item>
                        <Form.Item label="Type of delivery">
                            <Select
                                showSearch
                                style={{ width: '100%' }}

                                optionFilterProp="children"
                                onChange={this.onChangeDelivery}
                                onFocus={onFocus}
                                onBlur={onBlur}
                                onSearch={onSearch}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {delivery}
                            </Select>
                        </Form.Item>
                        <Form.Item label="price">
                            <Input name="price" value={price} onChange={this.onChange}/>
                        </Form.Item>
                        <FormItem label="address">
                            <Input value={adressOfOrder} name="adressOfOrder" onChange={this.onChange}/>
                        </FormItem>

                    </Form>
                </Modal>

            </div>
        )
    }
}
const mapStateToProps=(state)=>({
    coffee:state.coffee,
    syrup:state.coffee,
    size:state.coffee,
    delivery:state.coffee,
    modal:state.modal,
    auth:state.auth,
})
export default connect(mapStateToProps,{getCompany,getCoffes,getSizes,getDelivery,getSyrups,saveOrder}) (withRouter(Main));