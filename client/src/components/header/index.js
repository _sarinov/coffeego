import React,{Component} from 'react'
import Menu from "antd/es/menu";

import {Button,Icon} from "antd";
import './header.css'
import  Modal from '../modal'
import {connect} from "react-redux";
import {modalOpen} from "../../actions/modalActions";
import {Link,withRouter} from "react-router-dom";
import {logoutUser} from "../../actions/authActions";
const {SubMenu}=Menu
class  Header extends Component{

    constructor(){
        super();
        this.state={
            modalShow:false
        }
    }
    showModal=()=>{
        this.setState({modalShow:true})
    };
    hideModal=()=>{
        this.setState({modalShow:false})

    };
    logOut=()=>{
        this.props.logoutUser(this.props.history);
    }
render() {
    const {auth}=this.props;
    let header=''
    if(auth.isAuthenticated && localStorage.getItem('role')==='company'){
        header= <Menu>
            <SubMenu
                title={
                    <span style={{fontSize:"16px"}} className="submenu-title-wrapper">
                    <Icon type="setting" />
                     Settings
            </span>
                }
            >
                <Menu.Item key="setting:1"> <Link to={'/orders'}>My orders</Link></Menu.Item>
                <Menu.Item key="setting:2" onClick={this.logOut} >Log out</Menu.Item>
            </SubMenu>
        </Menu>
    }
    else if(auth.isAuthenticated && (localStorage.getItem('role')==='null' || localStorage.getItem('role')==='undefined')){
        header=
        <Menu>
            <SubMenu
                title={
                    <span style={{fontSize:"16px"}} className="submenu-title-wrapper">
                    <Icon type="setting" />
                     Settings
            </span>
                }
            >
               <Menu.Item key="setting:1"> <Link to={'/orders'}>My orders</Link></Menu.Item>
               <Menu.Item key="setting:2"> <Link  to={'/favorites'}>Favorites</Link></Menu.Item>
                <Menu.Item key="setting:3" onClick={this.logOut} >Log out</Menu.Item>
            </SubMenu>
        </Menu>
        {/*<span style={{display:"flex"}}><Link to={'/orders'}><Button  type="primary" style={{*/}
        {/*    background: " #c0aa83",*/}
        {/*    marginRight:"20px",*/}
        {/*    border: "none",*/}
        {/*    fontSize: "16px",*/}
        {/*    fontWeight: 500,*/}
        {/*    color: "#fff",*/}
        {/*    width: "150px",*/}
        {/*    height: "40px"*/}
        {/*}}>My orders</Button></Link> <Button onClick={this.logOut} type="primary" style={{*/}
        {/*    background: " #c0aa83",*/}
        {/*    border: "none",*/}
        {/*    fontSize: "16px",*/}
        {/*    fontWeight: 500,*/}
        {/*    color: "#fff",*/}
        {/*    width: "150px",*/}
        {/*    height: "40px"*/}
        {/*}}>Log out</Button> </span>*/}

    }
    else {
        header=<Button onClick={this.props.modalOpen} type="primary" style={{
            background: " #c0aa83",
            border: "none",
            fontSize: "16px",
            fontWeight: 500,
            color: "#fff",
            width: "150px",
            height: "40px"
        }}>Sign in/sign up</Button>
    }
    return (
        <header className="header">
            <Modal show={this.showModal} handleClose={this.hideModal}/>
            <div className="container">
                <div className="header-inner">
                    <div className="logo">
                        <img src="/assets/logo_white-7.png" alt="logo"/>
                    </div>

                    <Menu defaultSelectedKeys={['2']} mode="horizontal">
                        <Menu.Item key="2">
                            <Link style={{color:"#fff"}} to={'/'}>
                            Home
                            <Icon type="right"/>
                            </Link>
                        </Menu.Item>

                        <Menu.Item key="3">
                            <Link style={{color:"#fff"}} to={'/'}>
                            About us
                            <Icon type="right"/>
                            </Link>
                        </Menu.Item>
                        <Menu.Item key="4">
                            <Link style={{color:"#fff"}} to={'/'}>
                            Products
                            <Icon type="right"/>
                            </Link>
                        </Menu.Item>
                        <Menu.Item key="5">
                            <Link style={{color:"#fff"}} to={'/'}>
                            Gallery
                            <Icon type="right"/>
                            </Link>
                        </Menu.Item>
                        <Menu.Item key="6">
                            <Link style={{color:"#fff"}} to={'/'}>
                            Contacts
                            <Icon type="right"/>
                            </Link>
                        </Menu.Item>


                    </Menu>
                    {header}

                </div>
            </div>
        </header>
    )
}
}
const mapStateToProps=(state)=>({
auth:state.auth,

})
export default connect(mapStateToProps,{modalOpen,logoutUser}) (withRouter(Header));