import React, { Component } from 'react'
import {connect} from "react-redux";
import Tabs from "../../components/TabAuth/TabsAuth";
import {modalClose} from "../../actions/modalActions";
import classnames from "classnames";
import Register from "../../containers/auth/register/";
import Login from "../../containers/auth/login/";
import Icon from "antd/es/icon";
import './modal.css'


class Modal extends Component {
    constructor(){
        super();

    }


    Modal=()=>{
        this.props.modalClose();
    }
    render (){
        const {modal} = this.props.modal;
        console.log("header",this.props)
        return(
            <div className={classnames("modal",{"modal-active": modal})}  >

                <div className="modal-backdrop" onClick={this.Modal} />
                {/*<span ><img src="./img/component/cancel.svg"/></span>*/}
                <Icon className="modal-close" onClick={this.Modal} type="close" theme="outlined" style={{color:"#fff",width:"20px"}} />
                <div className="modal-inner-login" >
                    <Tabs >
                        <div label="sign in" >
                            <Login/>
                        </div>
                        <div label="sign up" >
                            <Register/>
                        </div>

                    </Tabs>
                </div>


            </div>

        )
    }
}


const mapStateToProps=(state)=>({
    modal:state.modal

})


export default connect(mapStateToProps, {modalClose})(Modal);