
import React from "react";
import { BrowserRouter as Router, Route, withRouter, Switch} from 'react-router-dom';

import Company from '../../containers/company'
import UserOrder from '../../containers/order'
function RouteCompany(){
    return(
        <div>
            <Router>
           <Company/>
            </Router>
        </div>
    )
}
export default RouteCompany;